import React, {Component} from 'react';
import {Text, View, TextInput, Button, Vibration, Linking} from 'react-native';
import {RNCamera} from 'react-native-camera';
import BarcodeMask from 'react-native-barcode-mask';
import styles from './style.js';

const Separator = () => <View style={styles.separator} />;

class BarcodeQRScannerProjekt extends Component {
  constructor(props) {
    super(props);
    this.camera = null;
    this.alertPresent = false;
    this.buttonDisabled = true;
    this.uebergabe = null;
    this.type = null;

    this.state = {
      barcode: '',
      camera: {
        type: RNCamera.Constants.Type.back,
        flashMode: RNCamera.Constants.FlashMode.auto,
      },
    };
  }

  newScan = () => {
    this.alertPresent = false;
    this.setState({barcode: ''});
    this.buttonDisabled = true;
  };

  webview = () => {
    let ean = this.uebergabe;
    let url = 'https://www.google.com/search?q=' + ean + '&tbm=shop';
    Linking.openURL(url).catch(err => console.error('Verbindungsfehler ', err));
  };

  onBarCodeRead = e => {
    this.buttonDisabled = false;
    if (!this.alertPresent) {
      this.alertPresent = true;
      setTimeout(() => {
        Vibration.vibrate(2); // Vibrations-Feedback sobald Code gescannt
        this.setState({barcode: e.data});
        this.type = e.type;
        this.uebergabe = e.data;
      }, 1500);
    }
  };

  //Fallunterscheidung ob der Barcode vom Typ QR-Code ist.
  BarcodeOrQr = () => {
    if (this.type === 'QR_CODE') {
      this.qrView();
    } else {
      this.webview();
    }
  };

  qrView() {
    let url = this.uebergabe;
    Linking.openURL(url).catch(err => console.error('Verbindungsfehler ', err));
  }

  render() {
    return (
      <View style={styles.container}>
        {/*Fremdcode-Quelle_1: react-native-camera URL: https://github.com/react-native-camera/react-native-camera--*/}
        <RNCamera
          ref={ref => {
            this.camera = ref;
          }}
          defaultTouchToFocus
          flashMode={this.state.camera.flashMode}
          mirrorImage={false}
          onBarCodeRead={this.onBarCodeRead.bind(this)}
          onFocusChanged={() => {}}
          onZoomChanged={() => {}}
          androidCameraPermissionOptions={{
            title: 'Erlaubnis die Kamera zu benutzen',
            message:
              'Es wird Ihre Erlaubnis benötigt um die Kamere benutzen zu können',
            buttonPositive: 'Ok',
            buttonNegative: 'Abbruch',
          }}
          style={styles.preview}
          type={this.state.camera.type}
        />
        {/*Fremdcode-Quelle_1: ENDE */}

        {/*Fremd-Code_2: react-native BarcodeMask URL: https://github.com/nartc/react-native-barcode-mask*/}
        <BarcodeMask lineAnimationDuration={2600} />
        {/*Fremd-Code_2: ENDE*/}

        <View style={[styles.overlay, styles.topOverlay]}>
          <Text style={styles.scanScreenMessage}>
            Bitte scanne einen gültigen Code
          </Text>
        </View>
        <View style={styles.lowerSection}>
          <TextInput
            style={styles.scanMessage}
            placeholder="Der Code des Objekts">
            {this.state.barcode}
          </TextInput>
          <Separator />
        </View>
        <View style={styles.lowerSection}>
          <Button title="Erneut scannen" onPress={this.newScan} />
          <View style={styles.space} />
          <Button
            color="green"
            title="Rufe Website auf"
            disabled={this.buttonDisabled}
            onPress={this.BarcodeOrQr}
          />
        </View>
      </View>
    );
  }
}

export default BarcodeQRScannerProjekt;
